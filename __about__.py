# region System

# системное имя модели, должно совпадать с проектом
NAME = 'logo'

# версия должна совпадать с тэгом в git
# первая цифра  (меняются веса) - глобальный рефакторинг
# вторая цифра (меняются веса) - добавление фич,
# третья цифра - фикс багов,
VERSION = '1.0.0'

# Описание весов {название: расширение}
WEIGHTS_DESCRIPTION = {
    'antispofing_densenet': '.pth',
}
# endregion

# region Swagger

# Переменная, которая описывает запрос в сваггере диспетчера
SUMMARY = 'nv-ml-smiths'

# Переменная, которая описывает запрос в сваггере диспетчера
REQUEST = {
    'images': {
        'type': 'string',
        'description': 'File to upload',
        'format': 'binary'
    }
}

# Переменная, которая описывает ответ в сваггере диспетчера
RESPONSE = {
    'task_id': {
        'type': 'string',
        'format': 'uuid4',
        'description': 'Internal id of request',
        'example': '0180ae28-418a-49e9-8415-0902c2bce049'
    },
    'predict_time': {
        'type': 'number',
        'description': 'Time in seconds for calculating current request',
        'example': 0.07922554016113281
    },
    'results': {
        'type': 'list',
        'description': 'Result response model',
        'example': [{
            'image_id': "be3de2ab-11cb-44ab-80f2-80ae79e2cbd0",
            'spoof_answer': 'fake',
            'probability': 0.5,
            'trt_mode': 'int8'
        }]
    },
    'results': {
        'type': 'list',
        'description': 'Result response model',
        'example': [{
            'image_id': "be3de2ab-11cb-44ab-80f2-80ae79e2cbd0",
            'boxes': [],
            'masks': [],
            'labels': [],
            'labels_names': [],
            'scores': [],
            'img': [],
            'mode': []
        }]
    }
}
# endregion
