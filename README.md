# CV-Brands

* [Документация]()

## Назначение
Проект детектирует бренды на 3 классов, MVP.

## Как запустить
```console
foo@bar:~/path$ git clone git@gitlab.com:neuro-core/clients/client-zefir/nv-ml-logo-recognition.git nv-ml-logo-recognition
foo@bar:~/path$ cd nv-ml-logo-recognition
foo@bar:~/path/nv-ml-logo-recognition$ docker-compose build
```
Меняем в зависимости от того, будем использовать Triton или обычную модель
```yaml
USE_TRITON: "false"
```
На
```yaml
USE_TRITON: "true"
```
Запускаем rabbit (docker-compose -f rabbit_filename.yaml up -d)
```yaml
version: '3'

services:
  dispatcher.prod.rabbit:
    container_name: dispatcher.prod.rabbit
    image: rabbitmq:3-management-alpine
    restart: always
    environment:
      RABBITMQ_DEFAULT_USER: neuro
      RABBITMQ_DEFAULT_PASS: Secure1337
    ports:
      - "5672:5672"
      - "15672:15672"

  dispatcher.prod.redis:
    container_name: dispatcher.prod.redis
    image: redis:alpine
    command: redis-server --requirepass SuperSecuredPasswordWhichNobodyWillEverHackLol1337
    ports:
      - "6379:6379"
    restart: always
```
Запускаем диспетчер, поменяв ip (dispatcher.prod.dispatcher/rabbit) на ip системы (docker-compose -f dispatcher_filename.yaml up -d)
```yaml
version: '3'

services:
  dispatcher.prod.redis:
    container_name: dispatcher.prod.redis
    image: redis:alpine
    command: redis-server --requirepass ""
    ports:
      - "6379:6379"
    restart: always

  dispatcher.prod.dispatcher:
    container_name: dispatcher.prod.dispatcher
    image: registry.gitlab.com/neuro-core/neuro-vision/nv-dispatcher:v1.4.7
    ports:
      - "7300:7300"
    restart: always
    environment:
      REQUEST_MAX_SIZE: 50
      SWAGGER_PATH: ''

      RABBIT_URI: ''

      REDIS_URI: ''
      RESULT_TTL: 1

      STATUS_TIMEOUT: 5

      NV_SECURITY_LOGIN: 'admin'
      NV_SECURITY_PASSWORD: 'admin'
```
Запускаем nv-ml-logo-recognition
```console
foo@bar:~/path/nv-ml-logo-recognition$ docker-compose up -d 
```
Запускаем Triton Inference Server
```console
foo@bar:~/path/nv-ml-logo-recognition$ docker run --gpus=1 --rm -p8000:8000 -p8001:8001 -p8002:8002 -v /full_path_to_weights_folder/weights/:/models nvcr.io/nvidia/tritonserver:21.06-py3 tritonserver --model-repository=/models
```
Если Triton запускать не нужно, не выполняем последнюю команду и меняем флаг в docker-compose.yaml

## Как обучить

```console
# установка зависимостей
foo@bar:~$ sudo apt update
foo@bar:~$ sudo apt install python3.8 python3.8-dev python3.8-venv
foo@bar:~$ sudo pip3 install virtualenv
foo@bar:~$ python3.8 -m venv ./venv
foo@bar:~$ source ./venv/bin/activate
(venv) foo@bar:~$ python3.8 -m pip install --upgrade pip

# установка проекта
(venv) foo@bar:~$ git@gitlab.com:neuro-core/clients/client-zefir/nv-ml-logo-recognition.git
(venv) foo@bar:~$ cd nv-ml-logo-recognition
(venv) foo@bar:~/nv-ml-logo-recognition$ git clone https://github.com/ultralytics/yolov5
(venv) foo@bar:~/nv-ml-logo-recognition$ cd yolov5
(venv) foo@bar:~/nv-ml-logo-recognition/yolov5$ pip3 install -r requirements.txt

### Формат датасета для обучения

* Исходный датасет должен быть преобразован из COCO формата (как загружался из CVAT) в YOLO-формат (подробнее https://github.com/ultralytics/yolov5/wiki/Train-Custom-Data), папка dataset (образец):
    - images
        - test
            - IMG_1.jpg
            - IMG_2.jpg
            - ...
        - train
            - IMG_7.jpg
            - IMG_8.jpg
            - IMG_9.jpg
            - ...
    - labels
        - test
            - IMG_1_LABELS.txt
            - IMG_2_LABELS.txt
            - ...
        - train
            - IMG_7_LABELS.txt
            - IMG_8_LABELS.txt
            - IMG_9_LABELS.txt
            - ...
* Создаем конфиг для трейна, путь: coco.yaml
    *  ```yaml
        path: dataset 
        train: images/train
        val: images/test

        # Classes
        nc: 5  # number of classes
        names: ['apple', 'coca-cola', 'mcdonalds']  # class names
        ```

* Запуск обучения: `(venv) foo@bar:~/yolov5 python3 train.py --img 640 --batch 16 --epochs 20 --data coco.yaml --weights yolov5l.pt`

* Метрики обучения записываются в папку `"runs/train/exp{номер запуска}"`
