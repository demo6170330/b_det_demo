import numpy as np
import requests

import io
from PIL import Image

import base64
from typing import Dict, List

import torch

import sys
import os

import subprocess

import time

sys.path.insert(0, './src/yolov5')

from src.yolov5.utils.augmentations import letterbox
from src.yolov5.utils.general import non_max_suppression, scale_coords
from src.yolov5.models.common import DetectMultiBackend, HostDeviceMem, allocate_buffers

import tritonhttpclient

from nv_ml_controller.ml_base_model import BaseMachineLearningModel, BaseModel
from nv_ml_controller.ml_task import BaseMlTask

from src.utils import str2bool

class REQUEST(BaseModel):
    images: List[str]

    class Config:
        title = 'REQUEST'
        extra = 'allow'

class RESPONSE(BaseModel):
    image_id: str
    boxes: list
    labels: list    
    labels_names: list
    scores: list

    class Config:
        title = 'RESPONSE'
        extra = 'forbid'


class Model(BaseMachineLearningModel):

    classes: Dict[int, str] = {
        0: "apple",
        1: "mcdonalds",
        2: "coca-cola",
    }

    base_images_size = (int(os.environ.get("BASE_IMAGE_SIZE", "640")), int(os.environ.get("BASE_IMAGE_SIZE", "640")))

    triton_client, yolo = None, None
    triton_input_names = ["images"]
    triton_output_names = ["output"]

    def model_init(self):

        device_type = os.environ.get("cuda_device", "cuda")
        if device_type == "cpu":
            self.device = torch.device('cpu')
        elif device_type == "cuda":
            torch.cuda.set_device(int(os.environ.get("CUDA_VISIBLE_DEVICES", "1")))
            
            self.device = torch.device(f'cuda:{os.environ.get("CUDA_VISIBLE_DEVICES", "1")}')
        else:
            raise ValueError("possible device types: (cpu, cuda)")

        self.conf_threshold = float(os.environ.get("conf_threshold", "0.25"))
        self.iou_threshold = float(os.environ.get("iou_threshold", "0.5"))
        self.max_detections = int(os.environ.get("max_detections", "1000"))
        self.brand_threshold_size = int(os.environ.get("brand_threshold_size", "100"))

        self.use_trt_weights = str2bool(os.environ.get("USE_TRT_WEIGHTS", "true"))
        self.max_batch_size = int(os.environ.get('TRT_MAX_BATCH_SIZE', '25'))

        if str2bool(os.environ.get("USE_TRITON", "false")):
            self.triton_client = tritonhttpclient.InferenceServerClient(url=os.environ.get("TRITON_URL", "0.0.0.0:8000"))
            flag = False
            while not flag:
                self.logger.info("Triton Inference Server health check...")
                try:
                    health_url = f'http://{os.environ.get("TRITON_URL", "0.0.0.0:8000")}/v2/health/ready'
                    self.logger.info('Triton health url: ' +  f'http://{os.environ.get("TRITON_URL", "0.0.0.0:8000")}/v2/health/ready')

                    response = requests.get(health_url)
                except:
                    self.logger.info(f"Can not ping Triton model by url: {health_url}, try to launch Triton Inference Server before model start, next ping after 5 seconds")
                else:
                    self.logger.info("Success ping: " + f'http://{os.environ.get("TRITON_URL", "0.0.0.0:8000")}/v2/health/ready')
                    self.logger.info(response.text)

                model_name, model_version = os.environ.get("TRITON_MODEL_NAME", "maskrcnn_onnx"), os.environ.get("TRITON_MODEL_VERSION", "1")
                self.logger.info(f"Triton model ping, model name: {model_name}, model version: {model_version}")
                try:
                    model_metadata = self.triton_client.get_model_metadata(
                        model_name=model_name, 
                        model_version=model_version
                    )
                    model_config = self.triton_client.get_model_config(
                        model_name=model_name, 
                        model_version=model_version
                    )
                except:
                    self.logger.info("Ping failed, check your model repository config, probably Triton Inference Server was launched but he hasn't seen the model")
                else:
                    self.logger.info(f"Model metadata: {model_metadata}")
                    self.logger.info(f"Model config: {model_config}")

                    flag = True
                time.sleep(5)
        else:
            if self.use_trt_weights:
                if not os.path.exists(self.config.weight_file.replace(".pt", ".engine")):
                    subprocess.run(f"python3 src/yolov5/export.py --weights {self.config.weight_file} --include engine --imgsz {self.base_images_size[0]} {self.base_images_size[1]} --device {os.environ.get('CUDA_VISIBLE_DEVICES', '1')} --batch-size {str(self.max_batch_size)} --dynamic --iou-thres {self.iou_threshold} --conf-thres {self.conf_threshold}", shell=True)
                
                if not os.path.exists(self.config.weight_file.replace(".pt", ".onnx")) or not os.path.exists(self.config.weight_file.replace(".pt", ".engine")):
                    self.logger.error("Error in trt file creation, can not create onnx file")
                else:
                    self.yolo = DetectMultiBackend(weights=self.config.weight_file.replace(".pt", ".engine"), dnn=False, device=self.device)
            else:
                self.yolo = DetectMultiBackend(weights=self.config.weight_file, dnn=False, device=self.device)

    def preprocessing(self, image: np.ndarray) -> torch.Tensor:
        shape = image.shape
        img = letterbox(image, self.base_images_size, stride=32, auto=False)[0]
        img = img.transpose((2, 0, 1))[::-1]  # HWC to CHW, BGR to RGB
        img = np.ascontiguousarray(img)
        img = torch.from_numpy(img)

        if not self.use_trt_weights:
            img = img.to(self.device)

        img = img.float()  # uint8 to fp16/32
        img /= 255
        return img, shape

    def filter_by_brand_size(self, bboxes:np.array) -> np.array:
        """
        Filter the face bounding boxes by face size
        
        :param bboxes: A list of bounding boxes, each one is a list of 4 numbers
        :type bboxes: list
        :param landmarks: whether to return facial landmarks in addition to bounding boxes
        :type landmarks: list
        :param scores: A list of float, the confidence scores of the detected faces
        :type scores: list
        :return: The bounding boxes, landmarks, and scores of the faces that are larger than the
        threshold size.
        """
        
        fiter_index = []
        for i, bbox in enumerate(bboxes):
            x1, y1, x2, y2 =  bbox
            if x2-x1 >= self.brand_threshold_size and y2-y1 >= self.brand_threshold_size:
                fiter_index.append(i)
        return fiter_index
    
    def postprocessing(self, preds: np.ndarray, shapes: List[tuple]) -> List[Dict[str, list]]:
        converted_preds = []
        preds = non_max_suppression(preds, self.conf_threshold, self.iou_threshold, None, False, self.max_detections)
        for i, det in enumerate(preds):
            if len(det):
                det = det.cpu().detach().numpy()
                det[:, :4] = scale_coords(self.base_images_size, det[:, :4], shapes[i]).round()

                labels_num = [i+1 for i in det[:, 5].astype(int).tolist()]
                pre_preds = {
                    "boxes": np.array(det[:, :4].astype(int).tolist()),
                    "labels": np.array(labels_num),
                    "labels_names": np.array([self.classes[label] for label in det[:, 5].astype(int).tolist()]),
                    "scores": np.array(det[:, 4].tolist())
                }
                filtered_index = self.filter_by_brand_size(pre_preds["boxes"])
                for k in pre_preds.keys():
                    pre_preds[k] = pre_preds[k][filtered_index].tolist()

                converted_preds.append(pre_preds)
            else:
                converted_preds.append({
                    "boxes": [],
                    "labels": [],
                    "labels_names": [],
                    "scores": []
                })
        return converted_preds

    def _get_triton_brand_prediction(self, batch: list) -> List[Dict[str, list]]:
        self.logger.info("Start Triton inference")
        if self.triton_client is None:
            raise ValueError("Tritonhttpclient is not initialized!")

        preprocessed = list(zip(*[self.preprocessing(image) for image in batch]))
        preprocessed_batch, shapes = torch.stack(list(preprocessed[0])).cpu().detach().numpy(), list(preprocessed[1]) 
        
        input0 = tritonhttpclient.InferInput(self.triton_input_names[0], preprocessed_batch.shape, 'FP32')
        input0.set_data_from_numpy(preprocessed_batch, binary_data=True)
        response = self.triton_client.infer(
            model_name=os.environ.get("TRITON_MODEL_NAME", "yolov5_onnx"), 
            model_version=os.environ.get("TRITON_MODEL_VERSION", "1"), 
            inputs=[input0], 
            outputs=[tritonhttpclient.InferRequestedOutput(name, binary_data=True) for name in self.triton_output_names],
            request_compression_algorithm='deflate',
            response_compression_algorithm='deflate',
        )

        output = torch.Tensor(np.array(response.as_numpy("output")))
        results = self.postprocessing(output, shapes)
        return results

    @torch.no_grad()
    def _get_yolo_brand_prediction(self, batch: list) -> List[Dict[str, list]]:
        self.logger.info("Start YOLOv5 inference")
        if self.yolo is None:
            raise ValueError("YOLOv5 is not initialized!")
        prep = time.time()
        batch_size = len(batch)
        preprocessed = list(zip(*[self.preprocessing(image) for image in batch]))
        preprocessed_batch, shapes = torch.stack(list(preprocessed[0])), list(preprocessed[1])
        self.logger.info(f'Preprocessing batch time: {time.time() - prep}')
        pred = time.time()

        if self.use_trt_weights and batch_size > self.max_batch_size:
            raise ValueError(f"TRT max batch size limit exceeded, current batch_size: {batch_size}, trt_max_batch_size: {self.max_batch_size}")

        output = self.yolo(preprocessed_batch)
        self.logger.info(f'Model predict batch time: {time.time() - pred}')
        t = time.time()
        results = self.postprocessing(output, shapes)
        self.logger.info(f'Postprocessing batch time: {time.time() - t}')
        return results
    
    def predict(self, task: REQUEST) -> List[RESPONSE]:
        results, batch = [], []
        self.logger.info('Inside predict()')
        for task_image in task.images:
            batch.append(task_image.b64)

        self.logger.info(f'Batch size {len(batch)}')
        
        if len(batch) != 0:
                start_decode_time = time.time()
                converted_batch = []
                for _, img_b64 in enumerate(batch):
                    image = np.array(Image.open(
                        io.BytesIO(base64.b64decode(img_b64))
                    ))
                    if image.shape[2] > 3:
                        image = image[:, :, 0:3]
                        
                    converted_batch.append(image)
                self.logger.info(f'Decode batch time: {time.time()-start_decode_time}')
                
                start_prediction_time = time.time()
                answer = None
                if str2bool(os.environ.get("USE_TRITON", "false")):
                    answer = self._get_triton_brand_prediction(converted_batch)
                else:
                    answer = self._get_yolo_brand_prediction(converted_batch)
                self.logger.info(f'Batch prediction time: {time.time()-start_prediction_time}')
                results.extend(answer)
        else:
            raise ValueError("Input batch is empty!")
        
        return results
    
    def health(self):
        pass
            
if __name__ == "__main__":
    Model()
