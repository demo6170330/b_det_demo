import os

import requests
import numpy as np
import matplotlib.pyplot as plt
import cv2
import io

import pickle
from tqdm import tqdm

import time

import argparse

def get_api_answer_from_image(image:np.ndarray, 
                              url:str='', 
                              login:str='admin', 
                              password:str='admin') -> dict:

    """
    Post request to api to get answer from image path

    :param image: image to recognize in rgb color space
    :param url: url to dispatcher
    :param login: login to dispatcher
    :param password: password to dispatcher
    :return result: dict
    """

    buf = io.BytesIO()
    plt.imsave(buf, image, format='png')
    image_data = buf.getvalue()

    files = {
            'images': image_data
    }
    data = {
    }

    answer = requests.post(url, files=files, data=data, auth=(login, password))

    json_answer = answer.json()
    answer_dict = dict()
    print(json_answer)
    result = json_answer.get('results')
    
    if json_answer.get('results'):
        answer_dict['task_id'] = json_answer['task_id']
        answer_dict['predict_time'] = json_answer['predict_time']
        answer_dict['boxes'] = result[0].get('boxes')
        answer_dict['img'] = result[0].get('img')
        answer_dict['labels_names'] = result[0].get('labels_names')
        answer_dict['scores'] = result[0].get('scores')
    return answer_dict

def parse_args():
    parser = argparse.ArgumentParser(description='Test requests module.')
    parser.add_argument('--url', type=str, default='',
                        help="Astro dispatcher request url", required=True)
    parser.add_argument('--login', type=str, default='admin',
                        help="Astro dispatcher request login", required=True)
    parser.add_argument('--password', type=str, default='admin',
                        help="Astro dispatcher request password", required=True)
    parser.add_argument('--test_dir', type=str, default='test_images',
                        help="Path to dir with test images", required=True)
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = parse_args()
    print(f"len images: {len(os.listdir(args.test_dir))}")
    times, full_times = [], []
    for i in tqdm(range(1), desc="requests time"):
        for path in tqdm(os.listdir(args.test_dir), desc="im iter"):

            tic = time.time()
            img = cv2.cvtColor(cv2.imread(os.path.join(args.test_dir, path)), cv2.COLOR_BGR2RGB)
            result = get_api_answer_from_image(
                img, 
                url=args.url,
                login=args.login,
                password=args.password
            )

            #if result.get("img"):
            #    time = result.get("predict_time")
            #    times.append(time)

            bboxes = result.get("boxes")
            labels = result.get("labels_names")
            scores = result.get('scores')

            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            print(bboxes, labels)
            for j, box in enumerate(bboxes):
                cv2.rectangle(img, tuple(box[:2]), tuple(box[2: ]), color=(0, 0, 255))
                cv2.putText(img, f"{labels[j]} {np.round(scores[j], 2)}", tuple(box[:2]), cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 255))
                
            cv2.imwrite(f"src/im/{path}", img)
            full_times.append(time.time() - tic)

    with open("astro_req_global.pkl", "wb") as f:
        pickle.dump(times, f)
    
    with open("astro_req_global_full.pkl", "wb") as f:
        pickle.dump(full_times, f)
