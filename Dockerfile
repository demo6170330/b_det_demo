FROM nvcr.io/nvidia/tensorrt:21.06-py3

# needed for access to privite requirements on gitlab.com
ARG secret_key

ENV TZ=Europe/Moscow
ENV CUDA_VISIBLE_DEVICES ${CUDA_VISIBLE_DEVICES}

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt update
RUN apt -y install python3 python3-pip
RUN apt -y install \
    git \
    libsm6 \
    libxext6 \
    libxrender-dev \
    libglib2.0-0 \
    libjpeg-dev \
    zlib1g-dev \
    libgl1-mesa-glx \
    ffmpeg \
    vim \
    nano \
    freeglut3-dev \
    make \
    tar \
    build-essential

# add access to private libs on gitlab.com
RUN git config --global url.https://gitlab-ci-token:${secret_key}@gitlab.com/.insteadOf ssh://git@gitlab.com/ 

# verify python
RUN /usr/bin/python3 -V

# install python requirements
COPY . /app

RUN /usr/bin/python3 -m pip install --no-cache-dir --upgrade pip
RUN /usr/bin/python3 -m pip install --no-cache-dir -r /app/requirements.txt

ENV PATH=/usr/local/cuda-11.1/bin:$PATH
ENV LD_LIBRARY_PATH=/usr/local/cuda-11.1/lib64:$LD_LIBRARY_PATH

RUN /usr/bin/python3 -m pip uninstall pycuda -y
WORKDIR /app/src/lib/pycuda-2021.1/
RUN /usr/bin/python3 configure.py --cuda-root=/usr/local/cuda
RUN make install

# copy all projects code and set WORKDIR
WORKDIR /app

# EntryPoint for docker continainer
ENTRYPOINT ["/usr/bin/python3"] 

# default command if no command was provided
# check "command" section in docker-compose.yaml
CMD ["-V"]
